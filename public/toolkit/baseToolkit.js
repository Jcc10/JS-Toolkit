/* esversion: 6 */

/* "I'll bould my own jQuery! Without bloat, and ES6 Compatable!" */
/* Base toolkit class, contains all the selector code. */

/**
 * This combines classes into one.
 * @example class tk extends aggregator(toolkit, contentToolkitExtention, styleToolkitExtention, urlToolkitExtention) {};
 * @param {Class} baseClass - The base class.
 * @param {...Class} mixins - Classes to mix in.
 * @return {Class} mixed class
 */
export const aggregator = (baseClass, ...mixins) => {
  let base = class _Combined extends baseClass {
    constructor (...args) {
      super(...args)
      mixins.forEach((mixin) => {
        mixin.prototype.initializer.call(this)
      })
    }
  }
  let copyProps = (target, source) => {
    Object.getOwnPropertyNames(source)
      .concat(Object.getOwnPropertySymbols(source))
      .forEach((prop) => {
        if (prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
          return
            Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop))
      })
  }
  mixins.forEach((mixin) => {
    copyProps(base.prototype, mixin.prototype)
    copyProps(base, mixin)
  })
  return base
}

/**
 * This is the base toolkit, all extentions are built off of this one.
 * @example let tk = new toolkit("body");
 * @see https://gitlab.com/Jcc10/JS-Toolkit
 */
export class toolkit{

    /**
     * Preforms the initial search.
     * @param {DOMString} [cssSearch] - The CSS you are searching for.
     * @param {Element} [baseElement=document] - the base element to start from
     * @param {boolean} [errorOnEmpty=false] - if a error should be thrown if no elements were selected when one needed to be.
     */
    constructor(cssSearch, baseElement, errorOnEmpty) {
      /**
       * Extentions in the current toolkit (and the order they were loaded).
       * @private
       * @type {String[]}
       */
      this.extentions = ["baseToolkit"];

      /**
       * If there are elements currently selected.
       * @protected
       * @type {boolean}
       */
      this.ele = false;

      /**
       * The elements currently selected.
       * @protected
       * @type {Array<Element>}
       */
      this.elements = [];

      /**
       * Error constants
       * @public
       * @type {ENUM}
       */
      this.ERRORS = {
        NO_ELEMENTS: "No Elements Selected",
        MISSING_REQUIREMENT: "Missing A Required Prerequisite Extention",
        INVALID_ENUM: "A Invalid ENUM was provided",
        INVALID_ELEMENT_COUNT: "A invalid count of selected elements were selected for the current function."
      }

      errorOnEmpty = errorOnEmpty || false;
      /**
       * If a error should be thrown when there are no elements to itterate through and elements are required for the operation.
       * @public
       * @type {boolean}
       */
      this.errorOnEmpty = errorOnEmpty;

      baseElement = baseElement || document;
      /**
       * If a error should be thrown when there are no elements to itterate through and elements are required for the operation.
       * @public
       * @type {Element}
       */
      this.baseElement = baseElement;

      // run the base search.
      this.searchBase(cssSearch);
    }

    /**
     * Adds extention to the extentions member.
     * @protected
     * @param {String} newExtention - the new extention to add to the array.
     */
    addExtention(newExtention){
      this.extentions.push(newExtention);
    }

    /**
     * Ensures a extention has already been loaded.
     * @protected
     * @param {String} requiredExtention - the extention that must already be in the toolkit.
     * @throws {this.ERROR.MISSING_REQUIREMENT}
     */
    requireExtention(requiredExtention){
      if(!(this.extentions.includes(requiredExtention))) {
        throw this.ERRORS.MISSING_REQUIREMENT;
      }
    }

    /**
     * Get's all current extentions.
     * @public
     * @return {String[]} all current extentions.
     */
    getExtentions(){
      return this.extentions;
    }

    /**
     * Checks to see if there are elements in the system.
     *
     * If there is not and it's set to throw errors, then it throws a error.
     * If there is not but it's not set to throw errors, it fails silently.
     * @throws {this.errors.NO_ELEMENTS} no elements when elements are required.
     * @public
     */
    hasElement(){
      if(!(this.ele) && this.errorOnEmpty) {
        throw this.errors.NO_ELEMENTS;
      }
    }

    /**
     * Updates the ele member.
     * @protected
     */
    updateEle(){
      if(this.elements.length > 0){
        this.ele = true;
      } else {
        this.ele = false;
      }
    }

    /**
     * Does a function on each element.
     * @public
     * @param {function} f - function to run on each element
     */
    doEach(f) {
      this.hasElement();
      this.elements.forEach(f);
    }

    /**
     * Returns this object's elements.
     * @public
     * @return {Array<Elements>}
     */
    getElements(){
      return this.elements;
    }

    /**
     * Imports elements from another toolkit.
     * @public
     * @param {toolkit} otherToolkit - the toolkit to import from.
     */
    importElementsFromToolkit(otherToolkit) {
      this.elements = otherToolkit.getElements;
      this.updateEle();
    }

    /**
     * Switches all elements to the parrents of the old list.
     * @public
     */
    parrents() {
      this.hasElement();
      let newElements = [];
      this.doEach((oldElement, index) => { newElements.push(oldElement.parentElement); });
      this.elements = newElements;
      this.updateEle();
    }

    /**
     * Searches current element list for new elements useing a CSS selector or Removes all CSS selectors.
     * @public
     * @param {DOMString} [cssSearch] - css to search for
     */
    searchChildren(cssSearch) {
      this.hasElement();
      if (cssSearch !== null){
        let newElements = [];
        let f = function(oldElement, index){
          newElements = newElements.concat(
              Array.prototype.slice.call(
                  oldElement.querySelectorAll(cssSearch)
                )
            );
        }
        this.doEach(f);
        this.elements = newElements;
        this.updateEle();
      } else {
        this.ele = false;
        this.elements = [];
      }
    }

    /**
     * Searches from the base Element for the CSS selector or Removes all selected elements.
     * @public
     * @param {DOMString} [cssSearch] - css to search for
     */
    searchBase(cssSearch) {
      if (cssSearch !== null){
        this.elements = this.baseElement.querySelectorAll(cssSearch);
        this.updateEle();
      } else {
        this.ele = false;
        this.elements = [];
      }
    }

}

/* esversion: 6 */

/**
 * Extention for makeing functional Moodrals.
 * @extends {toolkit}
 * @extends {styleToolkitExtention}
 * @extends {eventToolkitExtention}
 */
export class moodralToolkitExtention {

  /**
   * Checks if the required extentions were loaded and creates some members.
   * @public
   * @throws {String} If a required extention is missing at it's time of load.
   */
  initializer(){
    this.requireExtention("styleToolkitExtention");
    this.requireExtention("eventToolkitExtention");
    this.addExtention("httpConnectionsToolkitExtention");

    /**
     * Temporarry backup of element data
     * @private
     * @type {{ele: boolean, elements:Array<Element>}}
     */
    this.__tempBackup = {
      ele: false,
      elements: []
    }

    /**
     * The element(s) that contain the moodral.
     * @type {Array<Element>}
     */
    this.moodralBase = [];
  }

  /**
   * Selects the moodral and saves it. (GOES FROM THE BASE)
   * @public
   * @param {DOMString} cssSelector - The CSS selector FROM THE BASE.
   */
  setMoodralSelector(cssSelector){
    this.__saveTempBackup();
    this.searchBase(cssSelector);
    this.hasElement();
    this.moodralBase = this.__loadTempBackup();
  }

  /**
   * Adds a event handeler to open the moodral. (GOES FROM THE BASE)
   * @public
   * @param {DOMString} cssSelector - The CSS selector FROM THE BASE.
   */
  addMoodralOpener(cssSelector){
    this.__saveTempBackup();
    this.searchBase(cssSelector);
    let self = this;
    this.addListener("click", (d) => {self.openMoodral();});
    this.__loadTempBackup();
  }

  /**
   * Adds a event handeler to close the moodral. (GOES FROM THE BASE)
   * @public
   * @param {DOMString} cssSelector - The CSS selector FROM THE BASE.
   */
  addMoodralCloser(cssSelector){
    this.__saveTempBackup();
    this.searchBase(cssSelector);
    let self = this;
    this.addListener("click", (d) => {self.closeMoodral();});
    this.__loadTempBackup();
  }

  /**
   * Displays the moodral.
   * @public
   */
  openMoodral(){
    this.__saveTempBackup(this.moodralBase);
    this.changeStyle("display", "block");
    this.__loadTempBackup();
  }

  /**
   * Hides the moodral.
   * @public
   */
  closeMoodral(){
    this.__saveTempBackup(this.moodralBase);
    this.changeStyle("display", "none");
    this.__loadTempBackup();
  }

  /**
   * Saves current selector data in the backup member.
   * If both elements and ele are given, they will be sawped in.
   * @private
   * @param {Array<Element>} [elements] - Element list to swap to.
   */
  __saveTempBackup(elements){
    this.__tempBackup.elements = this.elements;
    this.__tempBackup.ele = this.ele;
    if(elements) {
      /**
       * All this stuff in a part of the parrent object, so we don't need to define it here.
       * @ignore
       */
      this.elements = elements;
      this.updateEle();
    }
  }

  /**
   * Loads current selector data from the backup member.
   * Returns the element selector.
   * @private
   * @return {Array<Element>}
   */
  __loadTempBackup(){
    let elements = this.elements;
    /**
     * All this stuff in a part of the parrent object, so we don't need to define it here.
     * @ignore
     */
    this.elements = this.__tempBackup.elements;
    let ele = this.ele;
    /**
     * All this stuff in a part of the parrent object, so we don't need to define it here.
     * @ignore
     */
    this.ele = this.__tempBackup.ele;
    return elements;
  }

};

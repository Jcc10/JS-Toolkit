/* esversion: 6 */

// Adds form tools to the toolkit

/**
 * A constant for if nothing
 * @type {String}
 * @todo finish the documentation for this one.
 */
export const NOTHING = "Nothing";

/**
 * Extention for working with forms.
 * @extends {toolkit}
 */
export class formToolkitExtention {

  /**
   * Sets up the members for this extention.
   * @public
   */
  initializer(){
    this.addExtention("formToolkitExtention");
    /**
     * Enum of options for the modification of the class.
     * @public
     * @constant
     * @type {ENUM}
     * @property {number} ON
     * @property {number} OFF
     * @property {number} TOGGLE
     */
    this.tri = {
      ON: 1,
      OFF: 0,
      TOGGLE: 0.5
    }
  }

  /**
   * Returns current value or sets value.
   * @public
   * @param {*|NOTHING} [value=NOTHING] - value to set.
   * @return {String|Array<String>} Current value.
   */
  value(value){
    this.hasElement();
    value = value || NOTHING;
    if (value === NOTHING) {
      let values = [];
      this.doEach(function (element, index) {
        values.push(element.value);
      });
      if (values.length == 1) {
        return values[0];
      } else {
        return values;
      }
    } else {
      this.doEach(function (element, index) {
        element.value = value;
      });
      return value;
    }
  }

  /**
   * Set's the valididy message and triggers it.
   * @public
   * @param {String} message - Error message to display.
   */
  setValididy(message) {
    this.hasElement();
    this.doEach( (element, index) => { element.setCustomValidity(message); });
  }

  /**
   * Display's warning message.
   * @public
   * @todo does this display a generic message or the last set one?
   */
  tellInvalid() {
    this.hasElement();
    this.doEach( (element, index) => { element.reportValidity(); });
  }

  /**
   * Toggles if a element is enabled.
   * @public
   * @param {ENUM} [status=TOGGLE] - If the status should be toggled, force enabled, or force disabled.
   */
  toggleEnabled(status) {
    this.hasElement();
    status = status || this.tri.TOGGLE;
    switch (status.toLowerCase()) {
      case this.tri.TOGGLE:
        this.elements.forEach( function (element, index) {
          if(element.disabled){
            element.disabled = false;
          } else {
            element.disabled = true;
          }
        });
        break;
      case this.tri.ON:
        this.elements.forEach( function (element, index) {
          element.disabled = false;
        });
        break;
      case this.tri.OFF:
        this.elements.forEach( function (element, index) {
          element.disabled = true;
        });
        break;
      default:
        throw this.ERROR.INVALID_ENUM;
    }
  }

};

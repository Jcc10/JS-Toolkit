# Jcc10's TOOLKIT

This is a more lightweight version of jQuery that is modular, can be imported and is fully ES6 compliant.

It is not intended to be a full replacement (yet), just to help me with making websites faster and cleaner.

# Extensions

The toolkit is split up into individual extensions both so you don't have to import everything and so that it's easer to maintain / update / add / find code.

To create a tk (local toolkit) use the aggregation function to combine the extentions you need into one class.

## baseToolkit
The base toolkit, handles finding stuff via CSS selectors and other rudimentary tasks. Must always be imported as the base toolkit.

## baseStorageToolkitExtention
Ensures that the storage exists and stuff like that. Never called directly.

## permanentStorageToolkitExtention
For saving stuff permanently

## sessionStorageToolkitExtention
For saving stuff for only a session.

## contentToolkitExtention
For modifying content of the DOM.

## styleToolkitExtention
For modifying the style of elements in the DOM. (Also for adding classes to elements)

## urlToolkitExtention
For interacting with the URL.

## eventToolkitExtention
For working with events.

## formToolkitExtention (Incomplete)
For working with HTML forms.
